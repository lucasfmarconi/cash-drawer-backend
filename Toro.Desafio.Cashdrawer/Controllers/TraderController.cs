﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class TraderController : ControllerBase
    {
        private readonly ITraderService _service;
        public TraderController(ITraderService service)
        {
            _service = service;
        }

        // GET: api/Trader/5
        [HttpGet("{id}", Name = "Get")]
        public Trader Get(int id)
        {
            return _service.GetById(id);
        }

        // POST: api/Trader
        [HttpPost]
        public void Post([FromBody] Trader trader)
        {
            _service.Create(trader);
        }
    }
}
