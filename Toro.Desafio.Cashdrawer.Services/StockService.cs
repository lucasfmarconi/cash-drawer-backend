﻿using System;
using System.Collections.Generic;
using System.Linq;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Services
{
    public class StockService : Service<Stock>, IStockService
    {
        private readonly IStockRepository _repository;
        private readonly ITraderService _traderService;
        private readonly IAccountService _accountService;
        public StockService(IAccountService accountService, ITraderService traderService, IStockRepository repository) : base(repository)
        {
            _repository = repository;
            _accountService = accountService;
            _traderService = traderService;
        }

        public override Stock GetById(object id)
        {
            return _repository.GetById(id);
        }

        public override void Create(Stock stock)
        {
            UpdateAccountBalance(stock);
            base.Create(stock);
        }

        public override void Update(Stock stock)
        {
            Stock workingStock = this.GetById(stock.Id);
            workingStock.CurrentAmmount = stock.CurrentAmmount;
            UpdateStockQuantity(workingStock, stock.Quantity);
            UpdateAccountBalance(workingStock);
            base.Update(workingStock);
        }

        private void UpdateStockQuantity(Stock workingStock, int quantityToUpdate)
        {
            workingStock.Quantity += quantityToUpdate;
        }

        private void UpdateAccountBalance(Stock stock)
        {
            Trader trader = _traderService.GetById(stock.Trader.Id);
            _accountService.UpdateAccountBalance(trader.Account, stock.CurrentAmmount);
        }

        public ICollection<Stock> GetStocksByTraderId(int traderId)
        {
            return _repository.Where(s => s.Trader.Id == traderId).ToList();
        }
    }
}
