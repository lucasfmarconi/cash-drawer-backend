FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY . .
RUN dotnet restore "Toro.Desafio.Cashdrawer/Toro.Desafio.Cashdrawer.csproj"
RUN dotnet build "Toro.Desafio.Cashdrawer/Toro.Desafio.Cashdrawer.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Toro.Desafio.Cashdrawer/Toro.Desafio.Cashdrawer.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Toro.Desafio.Cashdrawer.dll"]