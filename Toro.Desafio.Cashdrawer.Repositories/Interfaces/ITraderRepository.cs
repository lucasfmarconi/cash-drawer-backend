﻿using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Repositories.Interfaces
{
    public interface ITraderRepository : IRepository<Trader>
    {
    }
}
