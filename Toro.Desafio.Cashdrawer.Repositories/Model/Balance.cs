﻿namespace Toro.Desafio.Cashdrawer.Repositories.Model
{
    public class Balance
    {
        public decimal Current { get; set; }
        public Account Account { get; set; }
        public int AccountId { get; set; }
    }
}