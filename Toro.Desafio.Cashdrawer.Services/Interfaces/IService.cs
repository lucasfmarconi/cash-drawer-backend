﻿namespace Toro.Desafio.Cashdrawer.Services.Interfaces
{
    public interface IService<T> where T : class
    {
        void Create(T entity);
        T GetById(object id);
        void Update(T entity);
    }
}
