﻿using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Services
{
    public class Service<T> : IService<T> where T : class
    {
        private readonly IRepository<T> _repository;
        public Service(IRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual void Create(T entity)
        {
            _repository.Create(entity);
        }

        public virtual T GetById(object id)
        {
            return _repository.GetById(id);
        }

        public virtual void Update(T entity)
        {
            _repository.Update(entity);
        }
    }
}
