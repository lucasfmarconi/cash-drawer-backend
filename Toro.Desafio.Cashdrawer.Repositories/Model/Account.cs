﻿namespace Toro.Desafio.Cashdrawer.Repositories.Model
{
    public class Account
    {
        public int Id { get; set; }
        public Balance Balance { get; set; }
        public int TraderId { get; set; }
        public Trader Trader { get; set; }

        public bool IsValid(int traderId)
        {
            return traderId == this.TraderId;
        }
    }
}
