﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Repositories
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        private readonly MainContext _mainContext;
        public AccountRepository(MainContext context) : base(context)
        {
            _mainContext = context;
        }

        public override Account GetById(object id)
        {
            int accountId = Convert.ToInt32(id);

            Account account = _mainContext.Account.Include(a => a.Balance)
                .Where(a => a.Id == accountId).FirstOrDefault();

            return account;
        }
    }
}
