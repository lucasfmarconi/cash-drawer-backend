﻿using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Services.Interfaces
{
    public interface ITraderService : IService<Trader>
    {
    }
}
