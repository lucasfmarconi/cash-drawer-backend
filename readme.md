## DESAFIO TORO INVESTIMENTOS
### For backend solution
### RUNNING WITH DOCKER COMPOSE
1) On the root of this project run the following command
2) docker-compose.yml -f docker-compose.yml up -d
This command will get all of docker images this project needs and all of solutions will be run

### RUNNING WITH DOTNET withou docker and docker compose
##### CREATING DATABASE
1) mkdir "c:/temp/mysql/datadir"
2) run docker cmd
docker run -d --restart always -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=!Passw0rd" -p 1433:1433 -v c:\temp\sqlvolume:/var/opt/mssql --name mssql_container mcr.microsoft.com/mssql/server:2017-CU11-ubuntu
** create or change the c:\temp\sqlvolume path

##### RUNNING API
1) Check/Define global.json to set your dotnet core installed version (This solution was not tested with dotnet core 3.x)
2) Navigate to project root "Toro.Desafio.Cashdrawer"
3) On cmd, PS or BASH run > dotnet restore Toro.Desafio.Cashdrawer\Toro.Desafio.Cashdrawer.csproj
4) On your command line, define the CONNECTION_STRING env variable > set/export CONNECTION_STRING=Server=localhost;Database=cashdrawerdb;User=sa;password=!Passw0rd
5) dotnet run --project Toro.Desafio.Cashdrawer\Toro.Desafio.Cashdrawer.csproj

PS.: For a better experience, starting with POSTing data to "http://serverurl:port"/api/trader with the following payload:
{
	"name" : "Toro Trader"
}