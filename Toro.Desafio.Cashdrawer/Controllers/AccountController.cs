﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/trader/{traderId:int}/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _service;
        public AccountController(IAccountService service)
        {
            _service = service;
        }

        // PUT: api/trader/1/Account/1
        [HttpPut("{accountId}")]
        public Account Put([FromRoute]int traderId, [FromRoute]int accountId, [FromBody] Account account)
        {
            account.TraderId = traderId;
            account.Id = accountId;
            return _service.UpdateAccountBalance(account, account.Balance.Current);
        }
    }
}
