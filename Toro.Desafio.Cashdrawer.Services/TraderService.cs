﻿using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Services
{
    public class TraderService : Service<Trader>, ITraderService
    {
        private readonly ITraderRepository _repository;

        public TraderService(IRepository<Trader> baseRepository, ITraderRepository traderRepository) : base(baseRepository)
        {
            _repository = traderRepository;
        }

        public override Trader GetById(object id)
        {
            return _repository.GetById(id);
        }
    }
}
