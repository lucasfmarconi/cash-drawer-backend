﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Repositories
{
    public class TraderRepository : Repository<Trader>, ITraderRepository
    {
        private readonly MainContext _mainContext;
        public TraderRepository(MainContext context) : base(context)
        {
            _mainContext = context;
        }

        public override Trader GetById(object id)
        {
            int traderId = Convert.ToInt32(id);
            Trader trader = _mainContext.Trader.Include(t => t.Account).Where(t => t.Id == traderId).FirstOrDefault();
            trader.Account.Balance = _mainContext.Balance.Where(b => b.AccountId == trader.Account.Id).FirstOrDefault();
            return trader;
        }
    }
}
