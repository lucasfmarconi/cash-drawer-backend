﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toro.Desafio.Cashdrawer.Repositories;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services;
using Xunit;

namespace Toro.Desafio.Cashdrawer.Tests
{
    public class AccountTests
    {
        private MainContext _mainContext;
        private AccountRepository _accountRepository;
        private AccountService _accountService;
        public AccountTests()
        {
            InitContext();
        }

        [Obsolete]
        public void InitContext()
        {
            var builder = new DbContextOptionsBuilder<MainContext>().UseInMemoryDatabase();
            var context = new MainContext(builder.Options);
            _mainContext = context;

            Trader trader = new Trader()
            {
                Name = "Test New Trader"
            };
            _mainContext.Add(trader);
            _mainContext.SaveChanges();

            _accountRepository = new AccountRepository(_mainContext);
        }

        [Fact]
        public void CheckBalance200AfterFirstCashDeposit()
        {
            Trader newTrader = _mainContext.Trader.Include(t => t.Account).FirstOrDefault();
            newTrader.Account = _mainContext.Account.Include(t => t.Balance).FirstOrDefault();

            _accountService = new AccountService(_accountRepository);

            Assert.Equal(200, _accountService.UpdateAccountBalance(newTrader.Account, 200).Balance.Current);
        }

        [Fact]
        public void CheckAccountValidTrue()
        {
            Account account = new Account()
            {
                TraderId = 100
            };
            Assert.True(account.IsValid(100));
        }

        [Fact]
        public void CheckAccountValidFalse()
        {
            Account account = new Account()
            {
                TraderId = 101
            };
            Assert.False(account.IsValid(100));
        }
    }
}
