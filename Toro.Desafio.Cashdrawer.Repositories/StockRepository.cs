﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Repositories
{
    public class StockRepository : Repository<Stock>, IStockRepository
    {
        private readonly MainContext _mainContext;
        public StockRepository(MainContext context) : base(context)
        {
            _mainContext = context;
        }

        public override Stock GetById(object id)
        {
            int stockId = Convert.ToInt32(id);

            Stock stock = _mainContext.Stock.Include(s => s.Trader)
                .Where(a => a.Id == stockId).FirstOrDefault();

            return stock;
        }
    }
}
