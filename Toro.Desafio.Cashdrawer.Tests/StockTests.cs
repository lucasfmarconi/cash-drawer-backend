﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Toro.Desafio.Cashdrawer.Repositories;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services;
using Xunit;

namespace Toro.Desafio.Cashdrawer.Tests
{
    public class StockTests
    {
        private MainContext _mainContext;
        private StockRepository _stockRepository;
        private StockService _stockService;

        public StockTests()
        {
            InitContext();
        }

        [Obsolete]
        public void InitContext()
        {
            var builder = new DbContextOptionsBuilder<MainContext>().UseInMemoryDatabase();
            var context = new MainContext(builder.Options);
            _mainContext = context;
            _mainContext.Trader.Add(new Trader()
            {
                Name = "toro trader"
            });
            _mainContext.SaveChanges();

            _mainContext.Stock.Add(new Stock()
            {
                Trader = _mainContext.Trader.Find(1)
            });

            _stockRepository = new StockRepository(_mainContext);
        }

        [Fact]
        public void CheckStockValidTrue()
        {
            Trader toroTrader = new Trader() { Id = 1 };

            Stock stock = new Stock()
            {
                Trader = toroTrader
            };
            Assert.True(stock.IsValid(toroTrader.Id));
        }

        [Fact]
        public void CheckStockValidFalse()
        {
            Trader toroTrader = new Trader() { Id = 1 };

            Stock stock = new Stock()
            {
                Trader = toroTrader
            };
            Assert.False(stock.IsValid(20));
        }
    }
}
