﻿using Microsoft.EntityFrameworkCore;
using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Repositories
{
    public class MainContext : DbContext
    {
        public MainContext(DbContextOptions<MainContext> options) : base(options) { }

        public DbSet<Account> Account { get; set; }

        public DbSet<Balance> Balance { get; set; }

        public DbSet<Trader> Trader { get; set; }

        public DbSet<Stock> Stock { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Balance>(entity =>
            {
                entity.Property(b => b.Current).HasColumnType("decimal(10,2)");
                entity.HasKey(b => b.AccountId);
            });

            modelBuilder.Entity<Trader>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Name);
                entity.HasOne(t => t.Account).WithOne(a => a.Trader).HasForeignKey<Account>(t => t.TraderId);
            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.HasKey(s => s.Id);
                entity.Property(s => s.Id);
                entity.Property(s => s.Code);
                entity.Property(s => s.Quantity);
                entity.HasOne(s => s.Trader)
                    .WithMany(t => t.Stocks);
            });            

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(a => a.Id);
                entity.HasOne(a => a.Balance).WithOne(b => b.Account).HasForeignKey<Balance>(a => a.AccountId);
            });
        }
    }
}
