﻿using System;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Services
{
    public class AccountService : Service<Account>, IAccountService
    {
        private readonly IAccountRepository _repository;
        public AccountService(IAccountRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public Account UpdateAccountBalance(Account account, decimal ammountToUpdate)
        {
            Account workingAccount = _repository.GetById(account.Id);
            
            if (workingAccount.IsValid(account.TraderId))
                return UpdateBalance(workingAccount, ammountToUpdate);
            else
                throw new InvalidOperationException("Account not belongs to trader");
        }

        private Account UpdateBalance(Account workingAccount, decimal ammountToUpdate)
        {
            workingAccount.Balance.Current += ammountToUpdate;
            _repository.Update(workingAccount);
            return workingAccount;
        }
    }
}
