﻿using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Services.Interfaces
{
    public interface IAccountService : IService<Account>
    {
        Account UpdateAccountBalance(Account account, decimal ammountToUpdate);
    }
}
