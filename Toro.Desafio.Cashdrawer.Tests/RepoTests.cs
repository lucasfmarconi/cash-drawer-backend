﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Toro.Desafio.Cashdrawer.Repositories;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Xunit;

namespace Toro.Desafio.Cashdrawer.Tests
{
    public class RepoTests
    {
        private MainContext _mainContext;

        public RepoTests()
        {
            InitContext();
        }

        [Obsolete]
        public void InitContext()
        {
            var builder = new DbContextOptionsBuilder<MainContext>().UseInMemoryDatabase();
            var context = new MainContext(builder.Options);
            _mainContext = context;
            _mainContext.Trader.Add(new Trader()
            {
                Name = "toro trader"
            });
        }

        [Fact]
        public void CreateTraderWithAccountAndZeroBalance()
        {
            Trader trader = new Trader()
            {
                Name = "Test New Trader"
            };
            _mainContext.Add(trader);
            _mainContext.SaveChanges();
            Trader newTrader = _mainContext.Trader.Include(t => t.Account).FirstOrDefault();
            Account traderAccount = _mainContext.Account.Include(t => t.Balance).FirstOrDefault();
            Assert.Equal(0, traderAccount.Balance.Current);
        }

        [Fact]
        public void ReturnNumberOneTrader()
        {
            var trader = _mainContext.Trader.Find(1);
            Assert.Equal(1, trader.Id);
        }
    }
}
