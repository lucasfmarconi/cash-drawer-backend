﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Toro.Desafio.Cashdrawer.Repositories.Model
{
    public class Stock
    {
        public int Id { get; set; }

        public virtual Trader Trader { get; set; }

        public string Code { get; set; }

        public int Quantity { get; set; }
       
        [NotMapped]
        public decimal CurrentAmmount { get; set; }

        public bool IsValid(int traderId)
        {
            return this.Trader.Id == traderId;
        }
    }
}
