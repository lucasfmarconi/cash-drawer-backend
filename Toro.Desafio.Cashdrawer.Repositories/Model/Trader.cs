﻿using System.Collections.Generic;

namespace Toro.Desafio.Cashdrawer.Repositories.Model
{
    public class Trader
    {
        public Trader()
        {
            Account = new Account()
            {
                Balance = new Balance()
            };
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public Account Account { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }
    }
}
