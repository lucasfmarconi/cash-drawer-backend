﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;

namespace Toro.Desafio.Cashdrawer.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly MainContext _context;
        private readonly DbSet<T> _dbSet;
        public Repository(MainContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public virtual void Create(T entity)
        {
            _dbSet.Add(entity);
            Save();
        }

        public void Delete(object id)
        {
            T entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
            Save();
        }

        public virtual T GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            Save();
        }

        public ICollection<T> Where(Expression<Func<T, bool>> where)
        {
            return _dbSet.Where(where).ToList();
        }

        private void Save()
        {
            _context.SaveChanges();
        }
    }
}
