﻿using System.Collections.Generic;
using Toro.Desafio.Cashdrawer.Repositories.Model;

namespace Toro.Desafio.Cashdrawer.Services.Interfaces
{
    public interface IStockService : IService<Stock>
    {
        ICollection<Stock> GetStocksByTraderId(int traderId);
    }
}
