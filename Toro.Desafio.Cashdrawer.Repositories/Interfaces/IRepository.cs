﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Toro.Desafio.Cashdrawer.Repositories.Interfaces
{
    public interface IRepository<T> where T :class
    {
        void Create(T entity);
        T GetById(object id);
        void Delete(object id);
        void Update(T entity);
        ICollection<T> Where(Expression<Func<T, bool>> where);
    }
}
