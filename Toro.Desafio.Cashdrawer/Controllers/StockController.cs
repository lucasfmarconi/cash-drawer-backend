﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Toro.Desafio.Cashdrawer.Repositories.Model;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/trader/{traderId:int}/[controller]")]
    [ApiController]
    public class StockController : ControllerBase
    {
        private readonly IStockService _service;
        public StockController(IStockService service)
        {
            _service = service;
        }

        // GET: api/trader/1/Stock/200
        [HttpGet("{stockId}")]
        public Stock Get([FromRoute]int traderId, [FromRoute]int stockId)
        {
            Stock workingStock = _service.GetById(stockId);
            if (workingStock.IsValid(traderId))
                return workingStock;
            return new Stock(); //TODO: Implement Middleware exception to handle this kind of scenario
        }

        [HttpGet]
        public ICollection<Stock> Get(int traderId)
        {
            return _service.GetStocksByTraderId(traderId);
        }

        // POST: api/Trader/1/stock
        [HttpPost]
        public void Post([FromRoute]int traderId, [FromBody] Stock stock)
        {
            stock.Trader = new Trader() { Id = traderId };
            _service.Create(stock);
        }

        // PUT: api/Trader/1/stock/1
        [HttpPut("{stockId:int}")]
        public Stock Put([FromRoute]int traderId, [FromRoute]int stockId, [FromBody] Stock stock)
        {
            stock.Id = stockId;
            stock.Trader = new Trader() { Id = traderId };
            if(stock.IsValid(traderId))
            {
                _service.Update(stock);
                _service.GetById(stockId);
            }
            return new Stock(); //TODO: Implement Middleware exception to handle this kind of scenario
        }
    }
}
