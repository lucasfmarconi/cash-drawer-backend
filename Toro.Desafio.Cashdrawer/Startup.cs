﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using Toro.Desafio.Cashdrawer.Repositories;
using Toro.Desafio.Cashdrawer.Repositories.Interfaces;
using Toro.Desafio.Cashdrawer.Services;
using Toro.Desafio.Cashdrawer.Services.Interfaces;

namespace Toro.Desafio.Cashdrawer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    );
            });

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            RegisterRepositories(services);
            RegisterServices(services);

            services.AddDbContext<MainContext>(options => options.UseSqlServer(Configuration["CONNECTION_STRING"]));
        }

        private void RegisterServices(IServiceCollection services)
        {
            services.AddTransient(typeof(IService<>), typeof(Service<>));
            services.AddTransient<ITraderService, TraderService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IStockService, StockService>();
        }

        private void RegisterRepositories(IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ITraderRepository, TraderRepository>();
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IStockRepository, StockRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");
            app.UseMvc();

            UpdateDatabase(app);

        }

        private void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
            .GetRequiredService<IServiceScopeFactory>()
            .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<MainContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
